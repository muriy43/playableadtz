webpackHotUpdate("main",{

/***/ "./src/scripts/app/gameObjects/Hummer.js":
/*!***********************************************!*\
  !*** ./src/scripts/app/gameObjects/Hummer.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _PIXI = __webpack_require__(/*! PIXI */ "./node_modules/pixi.js/dist/esm/pixi.js");

var PIXI = _interopRequireWildcard(_PIXI);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Hummer = function (_PIXI$Sprite) {
    _inherits(Hummer, _PIXI$Sprite);

    function Hummer(texture) {
        _classCallCheck(this, Hummer);

        var _this = _possibleConstructorReturn(this, (Hummer.__proto__ || Object.getPrototypeOf(Hummer)).call(this, texture));

        _this.anchor.set(0, 1);
        _this._showTransition = 1;
        return _this;
    }

    _createClass(Hummer, [{
        key: 'show',
        value: function show() {
            var delay = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

            this.visible = true;
            this.showTransition = 0;
            this.alpha = 0;
            var timeLine = new TimelineMax({ onComplete: this.onAnimationComplete, onCompleteScope: this });
            timeLine.to(this, 0.5, { alpha: 1, showTransition: 0.2 });
            timeLine.play();
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.visible = false;
            this.showTransition = 0;
        }
    }, {
        key: 'showTransition',
        set: function set(value) {
            this.anchor.set(0, 1 - value);
            this._showTransition = value;
        },
        get: function get() {
            return this._showTransition;
        }
    }]);

    return Hummer;
}(PIXI.Sprite);

exports.default = Hummer;

/***/ })

})
//# sourceMappingURL=main.0613bc564259229e3c2e.hot-update.js.map