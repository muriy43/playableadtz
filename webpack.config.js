
var TerserPlugin = require('terser-webpack-plugin');
const { argv } = require('yargs');
const MODE = argv.MODE || 'development';
const isProduction = MODE === 'production';
const isMinify = isProduction;
const path = require('path');
module.exports = {
  mode: 'development',
  entry: './src/scripts/index.js',
  optimization: {
	  minimize: isMinify,
	  minimizer: [
		  new TerserPlugin({
			  parallel: true,
			  terserOptions: {
				  ecma: undefined,
				  warnings: false,
				  parse: {},
				  compress: {},
				  mangle: true,
				  module: false,
				  output: null,
				  toplevel: false,
				  nameCache: null,
				  ie8: false,
				  keep_classnames: undefined,
				  keep_fnames: true,
				  safari10: false,
			  },
		  }),
	  ],
	  removeAvailableModules: false,
	  removeEmptyChunks: false,
	  splitChunks: false,
  },

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
	rules: [
		{
			test: /.jsx?$/,
			include: [
					path.resolve(__dirname, 'src')
			],
			exclude: [
					path.resolve(__dirname, 'node_modules'),
			],
			loader: 'babel-loader',
			query: {
				presets: ['es2015']
		}
	}]
  },
  resolve: {
		extensions: ['.json', '.js', '.jsx', '.css'],
		alias: {
				'framework': path.join(__dirname, './src/scripts/framework'),
				'app': path.join(__dirname, './src/scripts/app'),
				'game': path.join(__dirname, './src/scripts/game'),
				'PIXI'      : path.join(__dirname, './node_modules/pixi.js'),
		}
  },
  devtool: 'source-map',
  devServer: {
		contentBase: __dirname,
		publicPath: path.join('/dist/'),
        watchContentBase: false,
        writeToDisk: true,
		port: 8080,
		allowedHosts: [
			'all'
		],
  }
};