import Signal from 'signals';

export default class EventsManager {
    constructor() {
        this.onMenuItemSelected = new Signal();
    }
}

EventsManager._getInstance = function() {
    if(!EventsManager._instance) {
        EventsManager._instance = new EventsManager();
    }

    return EventsManager._instance;
}

EventsManager.emitMessage = function(eventName, data) {
    EventsManager._getSignal(eventName).dispatch(data);
}

EventsManager.addHandler = function(eventName, handler) {
    EventsManager._getSignal(eventName).add(handler);
}

EventsManager._getSignal = function(eventName) {
    let eventMap = EventsManager._getEventsMap();
    let event = EventsManager._getEventsMap().get(eventName);
    if(!event) {
        event = new Signal();
        eventMap.set(eventName, event);
    }
    return event;
}

EventsManager._getEventsMap = function() {
    if(!EventsManager._eventsMap) {
        EventsManager._eventsMap = new Map();
    }
    
    return EventsManager._eventsMap;
}