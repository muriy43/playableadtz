import * as PIXI from 'PIXI';
import Device          from 'framework/system/Device';
import Logo from '../gameObjects/Logo';
import ContinueButton from '../gameObjects/ContinueButton';
import Hummer from '../gameObjects/Hummer';
import Menu from '../gameObjects/Menu';
import EventsManager from '../events/EventsManager';
import GameMessageType from '../type/GameMessageType';
import Stair from '../gameObjects/Stair';
import EndPopup from '../gameObjects/EndPopup';
import Austin from '../gameObjects/Austin';

export default class GameScreen extends PIXI.Container {
	constructor(app) {
		super();

        this.app = app;

		this.initChildren();
	}

	initChildren() {
		this.back = new PIXI.Sprite(PIXI.utils.TextureCache['back']);
		this.addChild(this.back);

		this.addDecoration("globl.png", {x: -39, y: 109});
		this.addDecoration("table.png", {x: 76, y: 196});
		this.addDecoration("sofa.png", {x: 1, y: 320});
		this.addDecoration("plant.png", {x: 330, y: -42});
		this.addDecoration("plant.png", {x: 1009, y: 164});
		this.addDecoration("bookStand.png", {x: 708, y: -28});

		this.austin = new Austin(PIXI.utils.TextureCache["Austin.png"]);
		this.austin.position.set(569, 419);
		this.addChild(this.austin);
		this.austin.playIdleAnimation();

		this.stair = new Stair(PIXI.utils.TextureCache["stairOld.png"], [
			PIXI.utils.TextureCache["newStair1.png"],
			PIXI.utils.TextureCache["newStair2.png"],
			PIXI.utils.TextureCache["newStair3.png"]
		]);

		this.stair.position.set(790, 54);
		this.addChild(this.stair);

		this.addDecoration("dec1.png", {x: 996, y: 438});

		this.hummer = new Hummer(PIXI.utils.TextureCache['iconHummer.png'], function(){this.showMenu()}.bind(this));
		this.hummer.position.set(961, 361);
		this.addChild(this.hummer);

		this.menu = new Menu();
		this.menu.position.set(871, 245);
		this.addChild(this.menu);

		this.endPoup = new EndPopup();
		this.addChild(this.endPoup);
		
		this.logo = new Logo(PIXI.utils.TextureCache['logo.png']);
		this.logo.position.set(238, 76);
		this.addChild(this.logo);
		
		this.button = new ContinueButton(PIXI.utils.TextureCache['button.png']);
		this.button.position.set(568, 570);
		this.addChild(this.button);
		this.button.playIdleAnimation();
	}

	addDecoration(textureName, position) {
		let decoration = new PIXI.Sprite(PIXI.utils.TextureCache[textureName]);
		decoration.position.set(position.x, position.y);
		this.addChild(decoration);
	}

	showMenu() {
		this.menu.show();
	}

	onShown() {
		this.logo.show();
		this.hummer.show(0.5);

		EventsManager.addHandler(GameMessageType.MENU_ITEM_SELECTED, function(data) {
			this.stair.changeDecoration(data.itemIndex);
		}.bind(this));
	}

	resize(w, h)
	{
		this.w = w;
        this.h = h;	
	}
}
