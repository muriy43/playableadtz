import * as PIXI from 'PIXI';
import TweenMax from 'gsap';

export default class ContinueButton extends PIXI.Sprite {
    constructor(texture) {
		super(texture);
        this.anchor.set(0.5, 0.5);

        
        this.interactive = true;
        this.buttonMode = true;
        
        this.click = this.tap = (event) => {
            console.log("click");
        }
	}

    playIdleAnimation() {
        this.animation = new TweenMax.to(this.scale, 1, {x: 1.1, y: 1.1, ease: Ease.easeInOut, yoyo: true, repeat: -1});
    }
}