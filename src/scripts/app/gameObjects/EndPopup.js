import * as PIXI from 'PIXI';
import TweenLite from 'gsap';
import EventsManager from '../events/EventsManager';
import GameMessageType from '../type/GameMessageType';

export default class EndPopup extends PIXI.Container {
    constructor() {
		super();
        
        this.initHandlers();
        this.initChildrens();

        this.visible = false;
	}

    initHandlers() {
        EventsManager.addHandler(GameMessageType.MENU_ITEM_CONFIRM, this.show.bind(this));
    }

    initChildrens() {
        this.fade = new PIXI.Sprite(PIXI.utils.TextureCache["finalFade.png"]);
        this.fade.width = 1136;
        this.fade.height = 640;
        this.fade.interactive = true;
        this.addChild(this.fade);

        this.poupContent = new PIXI.Sprite(PIXI.utils.TextureCache["final.png"]);
        this.poupContent.anchor.set(0.5)
        this.poupContent.position.set(568, 250);
        this.addChild(this.poupContent);
    }

    show() {
        this.visible = true;
        this.showTransition = 0;
        this.fade.alpha = 0;
        TweenLite.to(this, 1, {showTransition: 1, ease: Back.easeOut});
        TweenLite.to(this.fade, 0.2, {alpha: 1});
    }

    set showTransition(value) {
        this.poupContent.scale.set(value, value);
        this._showTransition = value;
    }

    get showTransition() {
        return this._showTransition;
    }
}