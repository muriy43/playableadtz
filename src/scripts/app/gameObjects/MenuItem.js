import * as PIXI from 'PIXI';
import ItemSelectedData from '../data/ItemSelectedData';
import EventsManager from '../events/EventsManager';
import GameMessageType from '../type/GameMessageType';

export default class MenuItem extends PIXI.Container {
    constructor(iconTexture, selectCallBack, menuIndex) {
        super();
        this._showTransition = 1;
        this.pivot.set(68.5);
        this.iconTexture = iconTexture;

        this.selectCallBack = selectCallBack;
        this.menuIndex = menuIndex;

        this.initChildren();

        this.visible = false;
    }

    initChildren() {
        this.back = new PIXI.Sprite(PIXI.utils.TextureCache['chooseBack.png']);
        this.addChild(this.back);

        this.choosed = new PIXI.Sprite(PIXI.utils.TextureCache['choosed.png']);
        this.choosed.position.set(8, 4);
        this.choosed.visible = false;
        this.addChild(this.choosed);

        this.icon = new PIXI.Sprite(this.iconTexture);
        this.icon.position.set(10, 7);
        this.addChild(this.icon);

        
        this.interactive = true;
        this.buttonMode = true;
        
        this.click = this.tap = function() {
            this.selectCallBack();
            this.select();
        };

        this.confirmButton = new PIXI.Sprite(PIXI.utils.TextureCache['okey.png']);
        this.confirmButton.position.set(-10, 105);
        this.confirmButton.visible = false;
        this.addChild(this.confirmButton);

        this.confirmButton.interactive = true;
        this.confirmButton.click = this.confirmButton.tap = function() {
            this.confirm();
        }.bind(this);
    }

    confirm() {
        EventsManager.emitMessage(GameMessageType.MENU_ITEM_CONFIRM);
    }

    select() {
        EventsManager.emitMessage(GameMessageType.MENU_ITEM_SELECTED, new ItemSelectedData(this.menuIndex));
        this.confirmButton.visible = true;
        this.choosed.visible = true;
    }

    deselect() {
        this.confirmButton.visible = false;
        this.choosed.visible = false;
    }

    show(delay = 0) {
        this.showTransition = 0; 
        this.visible = true;
        this.scale.set(0);
        TweenLite.to(this, 0.5, {showTransition: 1, ease: Back.easeOut, delay: delay});
    }

    set showTransition(value) {
        this.scale.set(value, value);
        this._showTransition = value;
    }

    get showTransition() {
        return this._showTransition;
    }
}