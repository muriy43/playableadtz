import * as PIXI from 'PIXI';

export default class Stair extends PIXI.Sprite {
    constructor(defaultTexture, textureMap) {
		super(defaultTexture);
        this.textureMap = textureMap;
	}

    changeDecoration(decorationIndex) {
        if(this.currentDecorationIndex === decorationIndex) {
            return;
        }
        this.currentDecorationIndex = decorationIndex;

        this.texture = this._textrue = this.textureMap[decorationIndex];
        this.alpha = 0;
        if(!this.defalutY) {
            this.defalutY = this.y;
        }
        let posY = this.y - 150;

        if(this.timeLine) {
            this.timeLine.kill();
            this.y = this.defalutY;
        }

        this.timeLine = new TimelineMax({onComplete: this.onAnimationComplete, onCompleteScope: this});
        this.timeLine.to(this, 0.3, {alpha: 1});
        this.timeLine.from(this, 0.4, {y: posY}, -0.1);
        this.timeLine.play();
    }
}