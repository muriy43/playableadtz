import * as PIXI from 'PIXI';
import TweenMax from 'gsap';

export default class Austin extends PIXI.Sprite {
    constructor(texture) {
		super(texture);
        this.anchor.set(0, 1);
	}

    playIdleAnimation() {
        this.animation = new TweenMax.to(this.scale, 1.5, {y: 1.01, ease: Ease.easeInOut, yoyo: true, repeat: -1});
    }
}