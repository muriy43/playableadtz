import * as PIXI from 'PIXI';
import TweenLite from 'gsap';

export default class Logo extends PIXI.Sprite {
    constructor(texture) {
		super(texture);
        this.anchor.set(0.5, 0.5);

        this.visible = false;
	}

    show(delay = 0) {
        this.visible = true;
        this.showTransition = 0; 
        TweenLite.to(this, 0.5, {showTransition: 1, ease: Back.easeOut, delay: delay});
    }

    set showTransition(value) {
        this.scale.set(value, value);
        this._showTransition = value;
    }

    get showTransition() {
        return this._showTransition;
    }
}