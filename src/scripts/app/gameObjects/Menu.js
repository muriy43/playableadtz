import { TextureCache } from '@pixi/utils';
import * as PIXI from 'PIXI';
import EventsManager from '../events/EventsManager';
import GameMessageType from '../type/GameMessageType';
import MenuItem from './MenuItem';

export default class Menu extends PIXI.Container {
    constructor(onSelect, onConfirm) {
        super();
        this.onSelect = onSelect;
        this.onConfirm = onConfirm;
        this.menuItems = [];

        this.initHandlers();
        this.initChildren();
    }

    initHandlers() {
        EventsManager.addHandler(GameMessageType.MENU_ITEM_CONFIRM, function(){this.visible = false}.bind(this));
    }

    initChildren() {
        let item1 = new MenuItem(TextureCache["01.png"], this.selectCallback.bind(this), 0);
        item1.position.set(-134, 98);
        this.menuItems.push(item1);
        this.addChild(item1);

        let item2 = new MenuItem(TextureCache["02.png"], this.selectCallback.bind(this), 1);
        item2.position.set(0, 0);
        this.menuItems.push(item2);
        this.addChild(item2);

        let item3 = new MenuItem(TextureCache["03.png"], this.selectCallback.bind(this), 2);
        item3.position.set(155, -17);
        this.menuItems.push(item3);
        this.addChild(item3);
        this.visible = false;
    }

    selectCallback() {
        for(let i = 0; i < this.menuItems.length; i++) {
            this.menuItems[i].deselect();

        }
    }

    show() {
        this.visible = true;

        for(let i = 0; i < this.menuItems.length; i++) {
            this.menuItems[i].show(i * 0.1);

        }
    }
}