import * as PIXI from 'PIXI';

export default class Hummer extends PIXI.Sprite {
    constructor(texture, onChilckHnadler) {
        super(texture);
        this.anchor.set(0, 1);

        this.onChickCallback = onChilckHnadler;

        this.interactive = true;
        this.buttonMode = true;
        
        this.click = this.tap = this.onChick;
        this.visible = false;
    }

    onChick() {
        this.visible = false;
        this.onChickCallback();
    }

    show(delay = 0) {
        this.visible = true;
        this.showTransition = 0; 
        this.alpha = 0;
        let timeLine = new TimelineMax({onComplete: this.onAnimationComplete, onCompleteScope: this});
        timeLine.to(this, 0.1, {alpha: 1}, delay);
        timeLine.to(this, 0.4, {showTransition: 0.2, ease: Bounce.easeOut}, delay -0.1);
        timeLine.play();
    }

    set showTransition(value) {
        this.anchor.set(0, 1 - value);
        this._showTransition = value;
    }

    get showTransition() {
        return this._showTransition;
    }
}